type Params = {
  bufferSize: number
  sampleRate: number
}

export class Synth {
  sampleIndex: number
  bufferSize: number
  sampleRate: number
  buffer: Float32Array

  constructor({ bufferSize, sampleRate }: Params) {
    this.sampleIndex = 0
    Object.assign(this, { bufferSize, sampleRate })
    this.buffer = new Float32Array(this.bufferSize)
  }

  getNextSamples() {
    for (let i = 0; i < this.bufferSize; i++) {
      // given sample index, generate
      const index = this.sampleIndex + i
      const cycleIndex = index % this.sampleRate
      const u = cycleIndex / this.sampleRate // [0:1]

      const s = Math.sin(u * Math.PI * 2) // * some maff // [0:1]
      // const s = Math.random()
      // const amp = s * 2 - 1 // [-1:1]
      const amp = s
      this.buffer[i] = amp
    }
    this.sampleIndex += this.bufferSize
    return this.buffer
  }
}
